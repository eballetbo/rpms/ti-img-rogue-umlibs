#!/bin/sh

# Usage: ./make-git-snapshot.sh [COMMIT] [DATE]
#
# to make a snapshot of the given tag/branch.  Defaults to commitid.
# Point env var REF to a local mesa repo to reduce clone time.

HEAD=${1:-$( cat commitid )}
DATE=${2:-$( date +%Y%m%d )}
DIRNAME=ti-img-rogue-umlibs-${DATE}git${HEAD}

echo REF ${REF:+--reference $REF}
echo HEAD $HEAD
echo DIRNAME $DIRNAME

rm -rf $DIRNAME

git clone ${REF:+--reference $REF} \
	https://git.ti.com/git/graphics/ti-img-rogue-umlibs.git $DIRNAME

GIT_DIR=$DIRNAME/.git git archive --format=tar --prefix=$DIRNAME/ $HEAD \
	| xz > $DIRNAME.tar.xz

rm -rf $DIRNAME

sha512sum --tag $DIRNAME.tar.xz > sources

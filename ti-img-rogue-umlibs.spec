%global debug_package %{nil}

%global gitdate 20231129
%global pvrversion 23.2.6460340
%global commitid d054392
%global platform j784s4

Name:           ti-img-rogue-umlibs-%{platform}
Version:        %{pvrversion}^git%{commitid}
Release:        %autorelease
Summary:        Userspace libraries for PowerVR Rogue GPU on TI SoCs

License:        TI-TFL
URL:            https://git.ti.com/cgit/graphics/%{name}
Source0:        ti-img-rogue-umlibs-%{gitdate}git%{commitid}.tar.xz

BuildRequires:  make

ExclusiveArch: aarch64

%description
This package contains the user mode libraries and binaries for the PowerVR Rogue GPU. These
user mode libraries are developed by Imagination Technologies (IMG). TI provides additional
enhancements and integration for Linux TI SoCs.

%package -n firmware-ti-img-rogue-%{platform}
Summary: firmware-ti-img-rogue-%{platform}-%{pvrversion} package
Group: ti-img-rogue-umlibs
%description -n firmware-ti-img-rogue-%{platform}

%prep
%autosetup -n ti-img-rogue-umlibs-%{gitdate}git%{commitid}

%install
make TARGET_PRODUCT=%{platform}_linux DESTDIR=%{buildroot} install
mv %{buildroot}/usr/lib %{buildroot}/usr/lib64

%files
/etc/OpenCL/vendors/IMG.icd
/usr/bin/hwperfbin2jsont
/usr/bin/ocl_extended_test
/usr/bin/ocl_unit_test
/usr/bin/pvr_memory_test
/usr/bin/pvrdebug
/usr/bin/pvrhtb2txt
/usr/bin/pvrlogdump
/usr/bin/pvrsrvctl
/usr/bin/pvrtld
/usr/bin/rgx_blit_test
/usr/bin/rgx_compute_test
/usr/bin/rgx_triangle_test
/usr/bin/rgx_twiddling_test
/usr/bin/rogue2d_fbctest
/usr/bin/rogue2d_unittest
/usr/bin/tqplayer
/usr/lib64/libGLESv1_CM_PVR_MESA.so
/usr/lib64/libGLESv1_CM_PVR_MESA.so.23.2.6460340
/usr/lib64/libGLESv2_PVR_MESA.so
/usr/lib64/libGLESv2_PVR_MESA.so.23.2.6460340
/usr/lib64/libPVROCL.so
/usr/lib64/libPVROCL.so.1
/usr/lib64/libPVROCL.so.23.2.6460340
/usr/lib64/libPVRScopeServices.so
/usr/lib64/libPVRScopeServices.so.23.2.6460340
/usr/lib64/libVK_IMG.so
/usr/lib64/libVK_IMG.so.1
/usr/lib64/libVK_IMG.so.23.2.6460340
/usr/lib64/libglslcompiler.so
/usr/lib64/libglslcompiler.so.23.2.6460340
/usr/lib64/libpvr_dri_support.so
/usr/lib64/libpvr_dri_support.so.23.2.6460340
/usr/lib64/libsrv_um.so
/usr/lib64/libsrv_um.so.23.2.6460340
/usr/lib64/libsutu_display.so
/usr/lib64/libsutu_display.so.23.2.6460340
/usr/lib64/libufwriter.so
/usr/lib64/libufwriter.so.23.2.6460340
/usr/lib64/libusc.so
/usr/lib64/libusc.so.23.2.6460340
/usr/share/vulkan/icd.d/powervr_icd.json
%license LICENSE
%doc README

%files -n firmware-ti-img-rogue-%{platform}
/lib/firmware/rgx.fw.36.53.104.796
/lib/firmware/rgx.sh.36.53.104.796

%changelog
* Mon Dec 07 2023 Dana Elfassy <delfassy@redhat.com> - 23.2.6460340-1
- Move GPU firmware to its own package
* Mon Nov 20 2023 Dana Elfassy <delfassy@redhat.com> - 23.2.6460340
- Initial RPM release for j784s4 platform
